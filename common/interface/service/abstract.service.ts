import { IService } from './interface.service';

export abstract class AService implements IService {
  _repo;
  constructor(repo) {
    this._repo = repo;
  }
  async add(input: any) {
    try {
      return await this._repo.insert(input);
    } catch (error) {
      throw error;
    }
  }
  async update(id: any, data: any) {
    try {
      return await this._repo.update(id, data);
    } catch (error) {
      throw error;
    }
  }
  async delete(id: any) {
    try {
      return await this._repo.delete(id);
    } catch (error) {
      throw error;
    }
  }
  async getById(id: any) {
    try {
      return await this._repo.select({ id });
      // if (data.data.length > 0) {
      //   return data;
      // } else {
      //   throw new HttpException('KHONG TIM THAY', HttpStatus.NOT_FOUND);
      // }
    } catch (error) {
      throw error;
    }
  }
  async select(input: any) {
    try {
      return await this._repo.select(input);
    } catch (error) {
      throw error;
    }
  }

  async search(input: any) {
    try {
      return await this._repo.search(input);
    } catch (error) {
      throw error;
    }
  }
}
