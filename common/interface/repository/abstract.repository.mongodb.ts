import { HttpException, HttpStatus } from '@nestjs/common';
import { ObjectId } from 'mongodb';
import { IBaseRepository, OutputSelect } from './interface.repository';

export abstract class AbstractRepositoryMongodb<T>
  implements IBaseRepository<T>
{
  constructor(private dataSource) {}
  async findOne(option: any) {
    try {
      return await this.dataSource.findOne(option);
    } catch (err) {
      throw new HttpException(err, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  async findById(id) {
    try {
      return await this.dataSource.findById(id);
    } catch (err) {
      throw new HttpException(err, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  async insert(input: T): Promise<T> {
    try {
      return await this.dataSource.save(input);
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_GATEWAY);
    }
  }
  async update(id: string, data: any): Promise<T> {
    try {
      const rs = await this.dataSource.findOneAndUpdate(
        { _id: new ObjectId(id) },
        { $set: data },
        { returnDocument: 'after' },
      );
      return rs.value;
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_GATEWAY);
    }
  }
  async delete(id: string): Promise<any> {
    try {
      const data = await this.dataSource.findOneAndUpdate(
        { _id: new ObjectId(id), status: true },
        { $set: { status: false } },
      );
      if (data.value) {
        return true;
      } else {
        return false;
      }
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_GATEWAY);
    }
  }
  async select(input: any) {
    try {
      const page = Number(input?.page) || 1;
      const size = Number(input?.size) || 100;
      const orderBy = input?.orderBy || { createAtTimestamp: 'DESC' };
      input['status'] = true;
      if (input.id) {
        input._id = new ObjectId(input.id);
      }
      delete input.page;
      delete input.size;
      delete input.orderBy;
      delete input.id;
      Object.keys(input).forEach((element) => {
        if (
          !isNaN(parseFloat(input[element])) &&
          element != '_id' &&
          !isObjectId(input[element])
        ) {
          input[element] = Number(input[element]);
        }
        if (typeof input[element] == 'object') {
          if (input[element].gte) {
            input[element].$gte = input[element].gte;
            delete input[element].gte;
          }
          if (input[element].lte) {
            input[element].$lte = input[element].lte;
            delete input[element].lte;
          }
          if (input[element].gt) {
            input[element].$gt = input[element].gt;
            delete input[element].gt;
          }
          if (input[element].lt) {
            input[element].$lt = input[element].lt;
            delete input[element].lt;
          }
        }
      });
      const [data, totalCount] = await Promise.all([
        this.dataSource.find({
          where: input,
          skip: Number((page - 1) * size || 0),
          take: Number(size || 100),
          order: orderBy,
        }),
        this.dataSource.count(input),
      ]);
      const output = new OutputSelect<T>(data);
      output.count = data.length;
      output.totalCount = totalCount;
      output.page = page;
      output.size = size;
      output.totalPage = Math.ceil(totalCount / size);
      return output;
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_GATEWAY);
    }
  }
}

function isObjectId(str) {
  // Biểu thức chính quy để so khớp với định dạng ObjectId
  const objectIdRegex = /^[0-9a-fA-F]{24}$/;

  return objectIdRegex.test(str);
}
