const PRODUCT_URL = process.env.PRODUCT_URL;
const ORDER_URL = process.env.ORDER_URL;

export const config = {
  'product-service': {
    config: {
      baseURL: `${PRODUCT_URL}`,
      timeout: 10000,
    },
    module: {
      menu: {
        config: {
          baseURL: '/menu',
        },
        function: {
          add: {
            method: 'post',
            url: () => ``,
          },
          'estimated-amount': {
            method: 'post',
            url: () => `/estimated-amount`,
          },
          edit: {
            method: 'put',
            url: ({ id }) => `/${id}`,
          },
          getList: {
            method: 'get',
            url: () => ``,
          },
          delete: {
            method: 'delete',
            url: ({ id }) => `/${id}`,
          },
          get: {
            method: 'get',
            url: ({ id }) => `/${id}`,
          },
          search: {
            method: 'get',
            url: () => `/search`,
          },
        },
      },
    },
  },
};
