import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DiscoveryDecoratorModule } from 'common/modules/discovery-decorator/discovery-decorator';
import { HealthModule } from 'common/modules/health/module';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { AddDefaultInterceptor } from 'common/interceptor/interceptor.add-default';
import { OrdersModule } from './modules/orders/module';
import { OrderDetailsModule } from './modules/order-details/module';

@Module({
  imports: [
    OrdersModule,
    OrderDetailsModule,
    HealthModule,
    DiscoveryDecoratorModule,
    TypeOrmModule.forRoot({
      type: 'mongodb',
      name: 'mongodb',
      url: process.env.MONGODB_DATABASE,
      entities: [],
      synchronize: true,
      autoLoadEntities: true,
    }),
  ],
  controllers: [],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: AddDefaultInterceptor,
    },
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply()
      .exclude({ path: '/health', method: RequestMethod.ALL })
      .forRoutes('*');
  }
}
