import { IBaseEntity } from 'common/interface/repository/interface.base.entities';
import { Column, Entity, Index, ObjectIdColumn } from 'typeorm';

/* Order Detail Status
Pending (Đang chờ xử lý): Đơn hàng mới được tạo nhưng chưa được xem xét hoặc xử lý bởi nhân viên nhà hàng. Đây là trạng thái ban đầu của mọi đơn hàng.
Confirmed (Đã xác nhận): Đơn hàng đã được nhân viên nhà hàng xem xét và xác nhận. Trong giai đoạn này, đơn hàng chuẩn bị được đưa vào quy trình chuẩn bị món ăn.
In Preparation (Đang chuẩn bị): Đơn hàng đang được đầu bếp hoặc nhân viên nhà bếp chuẩn bị. Trạng thái này cho thấy quá trình chuẩn bị món ăn đã bắt đầu.
Ready to Serve (Sẵn sàng phục vụ): Món ăn đã được chuẩn bị xong và sẵn sàng để phục vụ cho khách hàng. Đây là trạng thái trước khi món ăn được mang ra bàn.
Served (Đã phục vụ): Món ăn đã được mang ra và phục vụ tại bàn của khách hàng hoặc thông báo cho khách hàng nhận nếu là dịch vụ mang đi.
Completed (Hoàn thành): Đơn hàng đã được thực hiện xong và khách hàng đã hoàn tất bữa ăn. Đây là trạng thái cuối cùng của đơn hàng trong hệ thống.
Cancelled (Đã hủy): Đơn hàng bị hủy bởi khách hàng hoặc nhà hàng do các lý do như khách hàng thay đổi ý định, mặt hàng không có sẵn, v.v.  
On Hold (Tạm hoãn): Đơn hàng tạm thời không được xử lý do đang chờ thông tin bổ sung từ khách hàng hoặc vì một lý do nào đó cần được giải quyết trước khi tiếp tục.
Failed (Thất bại): Đơn hàng không thể hoàn thành do các vấn đề phát sinh như lỗi trong quá trình chuẩn bị, vấn đề với nguyên liệu, hoặc sự cố nhân viên.
*/

export interface IEntity extends IBaseEntity {
  orderId: object;
  menuId: object;
  quantity: number;
  totalAmount: number;
  specialRequests: object;
  orderTime: Date;
  orderDetailStatus: string;
  customerId: object; // người đặt đồ
  menuInfo: object;
  menuAddons: object;
}
@Entity({ name: 'order-details' })
// @Index(['restaurantId', 'displayOrder'], { unique: true })
export class EntityMongodb implements IEntity {
  @Column({ nullable: true })
  menuAddons: object;

  @Column({ nullable: true })
  menuInfo: object;

  @Column({ nullable: true })
  orderId: object;

  @Column({ nullable: true })
  menuId: object;

  @Column({ nullable: true })
  quantity: number;

  @Column({ nullable: true })
  totalAmount: number;

  @Column({ nullable: true })
  specialRequests: object;

  @Column({ nullable: true })
  orderTime: Date;

  @Column({ nullable: true })
  orderDetailStatus: string;

  @Column({ nullable: true })
  customerId: object;

  @Column({ nullable: true })
  restaurantId: object;

  @Column({ nullable: true })
  tableId: object;

  @Column({ nullable: true })
  staffId: object;

  @Column({ nullable: true })
  reservationsId: object;

  @Column({ nullable: true })
  orderStatus: string;

  @Column({ nullable: true })
  name: string;

  @Column({ nullable: true })
  note: string;

  @Column({ nullable: true })
  description: string;

  @Column({ nullable: true })
  status: boolean;

  @Column({ nullable: true })
  isActive: boolean;

  @Column({ nullable: true })
  updateAt: Date;

  @Column({ nullable: true })
  updateAtTimestamp: number;

  @Column({ nullable: true })
  updateBy: string;

  @Column({ nullable: true })
  createBy: string;

  @Column({ nullable: true })
  createAtTimestamp: number;

  @ObjectIdColumn()
  _id: object;

  @Column({ nullable: true })
  createAt: Date;
}
