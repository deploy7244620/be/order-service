import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CreateDto } from './dto/create.dto';
import { EditDto } from './dto/edit.dto';
import { GetListDto } from './dto/get-list.dto';
import { Service } from './service';
import { Request } from '../../../common/libs/request';

const rootFolder = __dirname.split('/').pop();

@Controller(rootFolder.toLocaleLowerCase())
@ApiTags(rootFolder)
export class ModuleController {
  constructor(private _service: Service) {}
  // giỏ hàng
  @Post('/cart')
  async cart(@Body() body) {
    const request = new Request('product-service');
    const data = await request.requestHTTP(
      'menu',
      'estimated-amount',
      {},
      body.carts,
      {},
    );

    const promises = body.carts.map(async (item) => {
      const promis = [];
      if (item.orderDetailId) {
        // nếu tồn tại orderDetailId thì là update orderDetail hoặc delete
        promis.push(
          this.edit(item.orderDetailId, {
            quantity: item.quantity,
            menuAddons: item.addons,
            updateAt: body.updateAt,
            updateAtTimestamp: body.updateAtTimestamp,
          }).then((rs) => {
            return rs;
          }),
        );
        if (item.quantity == 0) {
          //xóa orderDetail
          // return this.add(input);
          promis.push(this.delete(item.orderDetailId));
        }
      } else {
        const input = new CreateDto();
        input.orderId = body.orderId;
        input.menuId = item.menuId;
        input.quantity = item.quantity;
        input.menuAddons = item.addons;

        input.updateBy = body.updateBy;
        input.updateAt = body.updateAt;
        input.updateAtTimestamp = body.updateAtTimestamp;
        input.createAt = body.createAt;
        input.createAtTimestamp = body.createAtTimestamp;
        input.status = body.status;
        input.isActive = body.isActive;

        promis.push(
          this.add(input).then((rs) => {
            item.result = rs;
            item.orderDetailId = rs._id;
            return rs;
          }),
        );
      }
      return Promise.all(promis);
    });
    const rs = await Promise.all(promises);
    const orderInfo = await this.getOrderDetail({ orderId: body.orderId });
    return {
      orderId: body.orderId,
      carts: body.carts,
      'estimate-amount': data.result,
      orderDetailInfo: orderInfo.data,
    };
    // if (data.result.count > 0) {
    //   body.menuInfo = data.result.data[0] || {};
    // }
  }

  @Get('/getOrderDetail')
  async getOrderDetail(@Query() input) {
    const orderId = input.orderId || '';
    const data = await this._service.select({ orderId });
    return data;
  }

  @Get('/search')
  async search(@Query() input) {
    return await this._service.search(input);
  }

  @Post()
  async add(@Body() body: CreateDto) {
    body.orderDetailStatus = 'Pending';

    const request = new Request('product-service');
    const data = await request.requestHTTP(
      'menu',
      'get',
      {},
      {},
      { id: body.menuId },
    );
    if (data.result.count > 0) {
      body.menuInfo = data.result.data[0] || {};
    }

    return await this._service.add(body);
  }

  @Delete('/:id')
  async delete(@Param('id') p_id) {
    return await this._service.delete(p_id);
  }

  @Get()
  async getList(@Query() input) {
    return await this._service.select(input);
  }

  @Get('/:id')
  async get(@Param('id') p_id) {
    return await this._service.select({ id: p_id });
  }

  @Put('/:id')
  async edit(@Param('id') p_id, @Body() body) {
    return await this._service.update(p_id, body);
  }
}
