import {
  IsBoolean,
  IsNumberString,
  IsOptional,
  IsString,
} from 'class-validator';

export class CheckDomainPriceDto {
  @IsString()
  inputDomain;

  @IsNumberString()
  prepay;

  @IsBoolean()
  @IsOptional()
  is_renew;
}
