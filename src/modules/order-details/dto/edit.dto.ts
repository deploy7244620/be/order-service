import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString } from 'class-validator';

export class EditDto {
  @IsOptional()
  @IsNumber()
  @ApiProperty({ nullable: true })
  price: number;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  parentId: object;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  restaurantId: object;

  @IsOptional()
  @ApiProperty({ nullable: true })
  imageUrl: object;

  @IsOptional()
  @IsNumber()
  @ApiProperty({ nullable: true })
  displayOrder: number;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  name: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  note: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  description: string;
}
