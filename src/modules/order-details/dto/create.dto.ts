import { ApiProperty } from '@nestjs/swagger';
import {
  IsDateString,
  IsEmpty,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class CreateDto {
  @IsOptional()
  @ApiProperty({ nullable: true })
  menuAddons: object;

  @IsEmpty()
  @ApiProperty({ nullable: true })
  menuInfo: object;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ nullable: true })
  orderId: object;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ nullable: true })
  menuId: object;

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty({ nullable: true })
  quantity: number;

  @ApiProperty({ nullable: true })
  @IsOptional()
  specialRequests: object;

  @IsOptional()
  @ApiProperty({ nullable: true })
  @IsDateString()
  orderTime: Date;

  @IsString()
  @IsOptional()
  @ApiProperty({ nullable: true })
  customerId: object; // người đặt đồ

  @IsEmpty()
  @ApiProperty({ nullable: true })
  @IsOptional()
  totalAmount: number;

  @IsEmpty()
  @ApiProperty({ nullable: true })
  @IsOptional()
  orderDetailStatus: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  note: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  description: string;

  @IsOptional()
  @ApiProperty({ nullable: true })
  status: boolean;

  @IsOptional()
  @ApiProperty({ nullable: true })
  isActive: boolean;

  @IsOptional()
  @ApiProperty({ nullable: true })
  updateAt: Date;

  @IsOptional()
  @ApiProperty({ nullable: true })
  updateAtTimestamp: number;

  @IsOptional()
  @ApiProperty({ nullable: true })
  updateBy: string;

  @IsOptional()
  @ApiProperty({ nullable: true })
  createAt: Date;

  @IsOptional()
  @ApiProperty({ nullable: true })
  createAtTimestamp: number;

  @IsOptional()
  @ApiProperty({ nullable: true })
  createBy: string;
}
