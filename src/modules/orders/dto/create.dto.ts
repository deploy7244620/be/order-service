import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateDto {
  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  customerId: object; // userId của khách hàng

  @IsNotEmpty()
  @ApiProperty({ nullable: true })
  restaurantId: object; // quán ăn

  @IsNotEmpty()
  @ApiProperty({ nullable: true })
  tableId: object; // khách chưa đặt bàn. Đây là thông tin khách hàng đang ngồi

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  staffId: object; // nhân viên đặt bàn cho khách

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  reservationsId: object; // khách đã đặt bàn trước

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  orderStatus: string; //draf(đơn hàng mới tạo chưa ấn đặt)  pending(khách hàng chọn món xong và ấn đặt món) completed(khách hàng đã thanh toán) Cancelled(cửa hàng hủy toàn bộ đơn hàng)

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  note: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  description: string;

  @IsOptional()
  @ApiProperty({ nullable: true })
  status: boolean;

  @IsOptional()
  @ApiProperty({ nullable: true })
  isActive: boolean;

  @IsOptional()
  @ApiProperty({ nullable: true })
  updateAt: Date;

  @IsOptional()
  @ApiProperty({ nullable: true })
  updateAtTimestamp: number;

  @IsOptional()
  @ApiProperty({ nullable: true })
  updateBy: string;

  @IsOptional()
  @ApiProperty({ nullable: true })
  createAt: Date;

  @IsOptional()
  @ApiProperty({ nullable: true })
  createAtTimestamp: number;

  @IsOptional()
  @ApiProperty({ nullable: true })
  createBy: string;
}
