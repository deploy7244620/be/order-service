import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CreateDto } from './dto/create.dto';
import { EditDto } from './dto/edit.dto';
import { GetListDto } from './dto/get-list.dto';
import { Service } from './service';
import { Request } from '../../../common/libs/request';

const rootFolder = __dirname.split('/').pop();

@Controller(rootFolder.toLocaleLowerCase())
@ApiTags(rootFolder)
export class ModuleController {
  constructor(private _service: Service) {}

  @Get('/search')
  async search(@Query() input) {
    return await this._service.search(input);
  }

  @Post()
  async createOrder(@Body() body: CreateDto) {
    const input = {
      updateAt: new Date(),
      updateAtTimestamp: new Date().getTime(),
      createAt: new Date(),
      createAtTimestamp: new Date().getTime(),
      status: true,
      isActive: true,
      orderStatus: 'draf',
      customerId: body.customerId,
      tableId: body.tableId,
      restaurantId: body.restaurantId,
      reservationsId: body.reservationsId,
      staffId: body.staffId,
    };

    // TODO: kiểm tra bàn có được phép đặt không

    return await this._service.add(input);
  }

  // @Post()
  // async add(@Body() body: CreateDto) {
  //   if (body.orderStatus == null) {
  //     body.orderStatus = 'draf';
  //   }
  //   return await this._service.add(body);
  // }

  // @Delete('/:id')
  // async delete(@Param('id') p_id) {
  //   return await this._service.delete(p_id);
  // }

  @Get()
  async getList(@Query() input) {
    return await this._service.select(input);
  }

  @Get('/:id')
  async get(@Param('id') p_id) {
    return await this._service.select({ id: p_id });
  }

  // @Put('/:id')
  // async edit(@Param('id') p_id, @Body() body: EditDto) {
  //   return await this._service.update(p_id, body);
  // }
}
