import { IBaseEntity } from 'common/interface/repository/interface.base.entities';
import { Column, Entity, Index, ObjectIdColumn } from 'typeorm';

export interface IEntity extends IBaseEntity {
  customerId: object; // userId của khách hàng
  restaurantId: object; // quán ăn
  tableId: object; // khách chưa đặt bàn. Đây là thông tin khách hàng đang ngồi
  staffId: object; // nhân viên đặt bàn cho khách
  reservationsId: object; // khách đã đặt bàn trước
  orderStatus: string; //draf(đơn hàng mới tạo chưa ấn đặt)  pending(khách hàng chọn món xong và ấn đặt món) completed(khách hàng đã thanh toán) Cancelled(cửa hàng hủy toàn bộ đơn hàng)
}
@Entity({ name: 'orders' })
// @Index(['restaurantId', 'displayOrder'], { unique: true })
export class EntityMongodb implements IEntity {
  @Column({ nullable: true })
  customerId: object;

  @Column({ nullable: true })
  restaurantId: object;

  @Column({ nullable: true })
  tableId: object;

  @Column({ nullable: true })
  staffId: object;

  @Column({ nullable: true })
  reservationsId: object;

  @Column({ nullable: true })
  orderStatus: string;

  @Column({ nullable: true })
  name: string;

  @Column({ nullable: true })
  note: string;

  @Column({ nullable: true })
  description: string;

  @Column({ nullable: true })
  status: boolean;

  @Column({ nullable: true })
  isActive: boolean;

  @Column({ nullable: true })
  updateAt: Date;

  @Column({ nullable: true })
  updateAtTimestamp: number;

  @Column({ nullable: true })
  updateBy: string;

  @Column({ nullable: true })
  createBy: string;

  @Column({ nullable: true })
  createAtTimestamp: number;

  @ObjectIdColumn()
  _id: object;

  @Column({ nullable: true })
  createAt: Date;
}
